<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/17 0017 20:48
 * Email: brximl@163.com
 * Name: 财务系统公告API
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Fnc0010c;
use app\common\model\Fnc0020c;
use app\common\model\Fnc0030c;
use app\common\model\Fnc2000c;
use hyang\Util;

class Finance extends Api
{
    /**
     * 标签保存
     */
    public function tag_save(){
        $uid = getUserInfo('uid');
        if($uid){
            list($data,$mode,$map) = $this->_getSaveData();
            //debugOut([$data,$mode,$map]);
            if($mode == 'A'){
                if(empty($data['tag'])) return $this->FeekMsg('请求参数不完整!');
                $data['uid'] = $uid;
                $fnc = new Fnc0010c($data);
                // 数据重复性检测-> 标签不可重复(根据标签名称/隐私标识判断)
                if($data['private_mk'] == 'Y'){
                    if($fnc->where('tag',$data['tag'])->count()) return $this->FeekMsg('【'.$data['tag'].'】已经存在，无需重复添加或者转为私有!');
                }else{
                    if($fnc->where([
                        'tag' => $data['tag'],
                        'private_mk'    => 'N',
                        'uid'            => $uid
                    ])->count()) return $this->FeekMsg('您私有【'.$data['tag'].'】标签已经存在，无需重复添加!');
                }
                if($fnc->save()) return $this->FeekMsg('标签添加成功!',1);
                return $this->FeekMsg('标签添加失败!');
            }elseif ($mode == 'M'){
                $fnc = new Fnc0010c();
                if($fnc->save($data,$map)) return $this->FeekMsg('标签更新成功!',1);
                return $this->FeekMsg('标签更新失败');
            }elseif ($mode == 'D'){
                if($this->pushRptBack('fnc0010c',$map,'auto')) return $this->FeekMsg('标签删除成功!',1);
                return $this->FeekMsg('标签无法移除!');
            }
        }
        return $this->FeekMsg('请求参数无效!');
    }

    /**
     * 获取当前用户下的标签后公共标签： argv => page,num
     * @return bool|\think\response\Json
     */
    public function get_tags(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $page = request()->param('page');
        $num = request()->param('num');
        $page = ($page)? intval($page):1;
        $num = ($num)? intval($num):20;
        $fnc = new Fnc0010c();
        $data = $fnc
            ->alias('a')
            ->join('sys_user b','a.uid = b.uid')
            ->field(['a.*','date_format(`a`.`mtime`,\'%Y-%m-%d\')'=>'date','b.account as author'])
            ->where(['a.private_mk'=>'Y','a.uid'=>$uid])
            ->whereOr(['a.private_mk'=>'N'])
            ->page($page,$num)
            ->order('a.private_mk,a.mtime desc')
            ->select();
        $count = $fnc
            ->where(['private_mk'=>'Y','uid'=>$uid])
            ->whereOr(['private_mk'=>'N'])
            ->count();
        $allPage = Util::getAllPage($count,$num);
        return $this->FeekMsg([
            'result' => $data,
            'page'   => $page,
            'all_page' => $allPage,
            'count' => $count
        ]);
    }
    /**
     * 科目保存
     * @return \think\response\Json
     */
    public function subject_save(){
        $uid = getUserInfo('uid');
        if($uid){
            list($data,$mode,$map) = $this->_getSaveData();
            //debugOut([$data,$mode,$map]);
            if($mode == 'A'){
                if(empty($data['subject'])) return $this->FeekMsg('请求参数不完整!');
                $data['uid'] = $uid;
                $fnc = new Fnc0030c($data);
                // 数据重复性检测-> 标签不可重复(根据标签名称/隐私标识判断)
                if($data['private_mk'] == 'Y'){
                    if($fnc->where('subject',$data['subject'])->count())
                        return $this->FeekMsg('【'.$data['subject'].'】已经存在，无需重复添加或者转为私有!');
                }else{
                    if($fnc->where([
                        'subject' => $data['subject'],
                        'private_mk'    => 'N',
                        'uid'            => $uid
                    ])->count()) return $this->FeekMsg('您私有【'.$data['subject'].'】科目已经存在，无需重复添加!');
                }
                if($fnc->save()) return $this->FeekMsg('科目添加成功!',1);
                return $this->FeekMsg('科目添加失败!');
            }elseif ($mode == 'M'){
                $fnc = new Fnc0010c();
                if($fnc->save($data,$map)) return $this->FeekMsg('标签更新成功!',1);
                return $this->FeekMsg('科目更新失败');
            }elseif ($mode == 'D'){
                if($this->pushRptBack('fnc0030c',$map,'auto')) return $this->FeekMsg('科目删除成功!',1);
                return $this->FeekMsg('科目无法移除!');
            }
        }
        return $this->FeekMsg('请求参数无效!');
    }

    /**
     * 事务甲方 weui - picker 数据结构获取
     * @return bool|\think\response\Json
     */
    public function master_get(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $fncx = new Fnc0020c();
        $data = $fncx
            ->where('type','M0')
            ->whereOr('type','00')
            ->where(['uid'=>$uid,'use_mk'=>'Y'])
            ->field('listid as value,name as label')
            ->select()
        ;
        return $this->FeekMsg($data);
    }

    /**
     * 事务乙方 weui - picker 数据结构获取  2017年7月2日 星期日
     * 限制仅仅获取前30个
     */
    public function slave_get_picker()
    {
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $fncx = new Fnc0020c();
        $data = $fncx
            ->where(['uid'=>$uid,'use_mk'=>'Y'])
            ->field('listid as value,name as label')
            ->limit(30)
            ->select();
        return $this->FeekMsg($data);
    }

    /**
     * 事务乙方通过文本匹配 2017年7月2日 星期日
     */
    public function slave_check_picker(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $salve = request()->param('salve');
        if($salve){
            $fnc = new Fnc0020c();
            $data = $fnc
                ->field('listid as value,name as label')
                ->where('uid',$uid)
                ->where('name','like',"%$salve%")
                ->select()
                ;
            if(!$data) return $this->FeekMsg('未匹配到数据');
            return $this->FeekMsg($data);
        }
        return $this->FeekMsg('请求参数无效');
    }
    /**
     * 标签 weui - picker 数据结构获取  2017年7月2日 星期日
     */
    public function tagid_get_picker(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $fnc = new Fnc0010c();
        // 分别记载共有的和私有的
        $data = $fnc
            ->field('listid as value,tag as label')
            ->where(['private_mk'=>'N'])
            ->whereOr(['uid'=>$uid,'private_mk'=>'Y'])
            ->select()
        ;
        return $this->FeekMsg($data);
    }

    /**
     * 财务标签通过文本匹配后去id(最匹配的一个) 2017年7月2日 星期日
     */
    public function tag_check_get(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $tag = request()->param('tag');
        if($tag){
            $fnc = new Fnc0010c();
            // 公有标签
            $data = $fnc
                ->field('listid as value,tag as label')
                ->where(['private_mk'=>'N'])
                ->where('tag','like','%'.$tag.'%')
                ->select()
                ;
            if(!$data){ // 私有标签
                $data = $fnc
                    ->field('listid as value,tag as label')
                    ->where(['uid'=>$uid,'private_mk'=>'Y'])
                    ->where('tag','like','%'.$tag.'%')
                    ->select()
                ;
            }
            if($data) return $this->FeekMsg($data);
        }
        return $this->FeekMsg($tag? '【'.$tag.'】标签还没有设置':'请求参数无效');
    }

    /**
     * 计划 weui - picker 数据结构获取  2017年7月2日 星期日
     */
    public function splan_get_picker(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $fnc = new Fnc2000c();
        $data = $fnc
            ->field('plan_no as value,plan as label')
            ->where('uid',$uid)
            ->where('end_mk','N')
            ->limit(50)
            ->order('mtime desc')
            ->select();
        if($data) return $this->FeekMsg($data);
        return $this->FeekMsg('未能查找到【财务计划】项数据');
    }
}