<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/21 0021 23:49
 * Email: brximl@163.com
 * Name:
 */

namespace app\api\controller;


use app\common\controller\Api;
use app\common\model\Fnc1000c;
use app\common\model\Fnc1001c;
use hyang\Util;

class Faccount extends Api
{
    // 数据保存
    public function save(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        list($data,$mode,$map) = $this->_getSaveData('no');
        $data = Util::dataUnset($data,['master']);
        $fnc = new Fnc1000c();
        $data = Util::dataClear($data,['tag_id','slave_id','subject_id','src_plan_no']);
        if($mode == 'A'){
            $data['uid'] = $uid;
            $data['no'] = $fnc->getNoVal();
            if($fnc->save($data)) return $this->FeekMsg('记账成功！',1);
            return $this->FeekMsg('记账失败！');
            //debugOut($data);
        }elseif ($mode == 'M'){
            if($fnc->save($data,$map)) return $this->FeekMsg('账单更新成功！',1);
            return $this->FeekMsg('账单更新失败！');
        }elseif ($mode == 'D'){
            if($this->pushRptBack($fnc->getTable(),$map,'auto')) return $this->FeekMsg('账单删除成功！',1);
            return $this->FeekMsg('账单删除失败！');
        }
        return $this->FeekMsg('系统请求失败！');
    }
    // 明细数据保存
    public function detail(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        list($data,$mode,$map) = $this->_getSaveData('no');
        $fnc = new Fnc1001c();
        if($mode == 'A'){
            // 唯一性检测
            if($fnc->where(['src_no'=>$data['src_no'],'name'=>$data['name']])->count()){
                return $this->FeekMsg('【'.$data['name'].'】账单明细已经存在！');
            }
            $data['no'] = $fnc->getNoVal();
            if($fnc->save($data)) return $this->FeekMsg('账单明细新增成功！',1);
            return $this->FeekMsg('账单明细新增！');
        }elseif ($mode == 'M'){
            if($fnc->save($data,$map)) return $this->FeekMsg('账单明细更新成功！',1);
            return $this->FeekMsg('账单明细更新失败！');
        }elseif ($mode == 'D'){
            if($this->pushRptBack($fnc->getTable(),$map,'auto')) return $this->FeekMsg('账单明细新增成功！',1);
            return $this->FeekMsg('账单明细新增失败！');
        }
        return $this->FeekMsg('系统请求失败！');
    }
    // 单条明细账
    // 参数： no * -> json
    public function get_detail(){
        $no = request()->param('no');
        if($no){
            $rs = (new Fnc1001c())->get($no)->toArray();
            return $this->FeekMsg($rs);
        }
        return $this->FeekMsg('请求参数无效！');
    }
    /**
     * 获取财务账单
     * 请求参数： page: 页码, num: 单列显示数
     */
    public function sets(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $page = request()->param('page');
        $num = request()->param('num');
        $page = is_numeric($page)? intval($page): 1;
        $num = is_numeric($num)? intval($num): 20;
        list($sets,$count) = (new Fnc1000c())->getFinanceSets($page,$num);
        return $this->FeekMsg([
            'result' => $sets,
            'page'   => $page,
            'num'   => $num,
            'count' => $count
        ]);
    }

    /**
     * 账单搜索
     */
    public function search(){
        $check = $this->needLoginNet($uid);
        if($check) return $check;
        $num = 50;$where = [];
        $search = request()->param('search');
        if($search) $search = trim($search);
        // 前缀匹配
        if(substr_count($search,'=') > 0){
            $idx = strpos($search,'=');
            $type = substr($search,0,$idx - 1);
            $template = [
                'mc' => 'a.name',
                'm'   => 'a.name',
                'rq' => 'a.date',
                'r'   => 'a.date',
                'ms' => 'a.descrip',
                'je' => 'a.money',
                'j' => 'a.money'
            ];
            $value = substr($search,$idx+1);
            if(isset($template[$type])) $where = [$template[$type]=>['like',"%$value%"]];
        }
        // 金额支持： <20,=<20,
        elseif (preg_match('/[\<\>=]+[\d.]/',$search)){
            preg_match('/[\d.]+/',$search,$match);
            $value = count($match)>0 && !empty($match[0])? $match[0]:'';
            $value = trim($value);
            if($value) $where = ['a.money'=>[str_replace($value,'',$search),"$value"]];
            else ['a.name'=>['like','%'.$search.'%']];
        }
        // 为全数字时自动匹配为金额
        elseif (preg_match('/[\d.]/',$search)) $where = ['a.money'=>['like',"%$search%"]];
        else $where = ['a.name'=>['like','%'.$search.'%']];
        $fnc = new Fnc1000c;
        $fnc1 = new Fnc1001c;
        $subSql = $fnc1->field('count(*)')->where('`src_no` = `a`.`no`')->buildSql();
        $data = $fnc
            ->alias('a')
            ->field(['a.*',$subSql=>'detail_ctt','DATE_FORMAT(date,\'%w\') as week'])
            ->where('a.uid',$uid)
            ->where($where)
            ->order('a.date desc,a.mtime desc')
            ->limit($num)
            ->select()
        ;
        $count = $fnc
            ->alias('a')
            ->where('a.uid',$uid)
            ->where($where)
            ->count();
        ;
        return $this->FeekMsg([
            'result' => $data,
            'count' => $count
        ]);
        return $data;
    }
}