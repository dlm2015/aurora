<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/22 0022 0:07
 * Email: brximl@163.com
 * Name:
 */

namespace app\common\model;


use think\Model;

class Fnc1000c extends Model
{
    protected $table = 'fnc1000c';
    protected $pk = 'no';
    // 获取主键
    public function getNoVal(){
        return getPkValue('pk_fnc1000c__no');
    }
    // 获取一条数据记录
    public function getOneList($no){
        $data = $this->db()
            ->alias('a')
            ->join(['fnc0020c' => 'b'],'a.master_id=b.listid')
            ->join(['fnc0010c' => 'c'],'a.tag_id=c.listid','left')
            ->join(['fnc2000c' => 'd'],'a.src_plan_no=d.plan_no','left')
            ->field('a.*,b.name as master, c.tag as tagid_desc,d.plan as splanno_desc')
            ->where('a.no',$no)
            ->find();
        if($data) $data = $data->toArray();
        return $data;
    }
    /**
     * 获取数据记录
     * @param int $page
     * @param int $num
     * @return ($data,$count)
     */
    public function getFinanceSets($page=1,$num=20){
        $uid = getUserInfo('uid');
        if(empty($uid)) return [null,0];
        $subSql = (new Fnc1001c)->field('count(*)')->where('`src_no` = `a`.`no`')->buildSql();
        $data = $this->db()
            ->alias('a')
			->join(['fnc0020c' => 'b'],'a.master_id=b.listid')
            ->join(['fnc0010c' => 'c'],'a.tag_id=c.listid','left')
            ->join(['fnc2000c' => 'd'],'a.src_plan_no=d.plan_no','left')
			->join(['fnc0030c' => 'e'],'a.subject_id=e.listid','left')
			->join(['fnc0020c' => 'f'],'a.slave_id=f.listid','left')
            ->field(['a.*,b.name as master, c.tag as tagid_desc,d.plan as splanno_desc,e.subject',$subSql=>'detail_ctt','DATE_FORMAT(date,\'%w\') as week','ifnull(f.name, a.slave)'=>'slave'])
            ->where('a.uid',$uid)
            ->order('a.date desc,a.mtime desc')
            ->page($page,$num)
            ->select()
        ;
        $count = $this->db()->where('uid',$uid)->count();
        return [$data,$count];
    }
}