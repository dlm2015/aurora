<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/23 0023 23:01
 * Email: brximl@163.com
 * Name: 财务计划纪事(财务计划驱动)
 */

namespace app\common\model;


use think\Model;

class Fnc2001c extends Model
{
    protected $table = 'fnc2001c';
    protected $pk = 'listid';
}