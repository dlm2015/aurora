<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/23 0023 22:54
 * Email: brximl@163.com
 * Name: 财务纪事
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc2001c;
use hyang\Bootstrap;

class Fevent extends Wap
{
    // 首页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '财务纪事'
        ]);
        $uid = $this->getUserInfo('uid');
        // 数据展示方式可选： 按条数/天数/月数/年等显示
        $fnc = new Fnc2001c();
        $data = $fnc
            ->alias('a')
            ->field(['a.title','a.mtime','a.descrip','a.listid',
                '(select count(*) from fnc2000c where pid=a.listid)'=>'plan_count'])
            ->where('a.uid',$uid)
            ->limit(10)
            ->select()
        ;
        $dataList = '';
        foreach ($data as $v){
            /*
            $dataList .= '
            <a class="weui-cell weui-cell_access" href="'.url('fevent/edit','item='.$v['listid']).'">
                <div class="weui-cell__bd">
                    <p>'.$v['title'].'</p>
                </div>
                <div class="weui-cell__ft">'.$v['mtime'].'</div>
            </a>
            ';
            */
            $dataList .= '
            <div class="weui-panel__bd">
                <div class="weui-media-box weui-media-box_text">
                    <h4 class="weui-media-box__title">'.$v['title'].'</h4>
                    <p class="weui-media-box__desc">('.$v['mtime'].') '.$v['descrip'].'</p>
                    <ul class="weui-media-box__info">
                        <li class="weui-media-box__info__meta"><a href="'.url('fevent/edit','item='.$v['listid']).'">编辑</a></li>
                        <li class="weui-media-box__info__meta">
                            <a href="'.urlBuild('!.fplan/edit','?ref_pid='.$v['listid']).'">
                                新增计划 '.($v['plan_count']? '<span class="weui-badge">'.$v['plan_count'].'</span>':'').'
                            </a>
                            </li>
                    </ul>
                </div>
                <div class="weui-flex aurora-border"></div>
            </div>
            ';
        }
        if($dataList) $dataList = '<div class="weui-panel" style="min-height: 420px;">'.$dataList.'</div>';
        $dataList = $dataList? $dataList:'
        <div class="weui-cells" style="min-height: 420px;">
            <a class="weui-cell weui-cell_access" href="'.url('fevent/edit').'">
                <div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>
                <div class="weui-cell__ft">
                    您还没一条财务计划记录
                </div>
            </a>
        </div>
        ';
        $page = [];
        $page['list'] = $dataList;
        $this->assign('page',$page);
        return $this->fetch();
    }
    // 编辑页面
    public function edit(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '编辑 | 财务纪事',
            'js'    => ['/lib/zepto/touch','fevent/edit']
        ]);
        $item = request()->param('item');
        $uid = $this->getUserInfo('uid');
        if($item){
            $fnc = new Fnc2001c();
            $data = $fnc->get($item)->toArray();
            if($uid != $data['uid']) $this->getErrorUrl();
            $this->assign('data',$data);
            $this->assign('f_pk_grid',Bootstrap::formPkGrid($data,'listid'));
        }
        return $this->fetch();
    }
}