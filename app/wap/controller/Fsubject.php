<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/21 0021 21:27
 * Email: brximl@163.com
 * Name:
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc0030c;

class Fsubject extends Wap
{
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '科目管理',
            'js'    => ['/lib/zepto/touch','fsubject/index']
        ]);
        $fnc = new Fnc0030c();
        $tPage = request()->param('page');
        $tPage = is_int($tPage)? $tPage:1;
        $num = 20;
        $page = [];
        $uid = $this->getUserInfo('uid');
        $data = $fnc
            ->alias('a')
            ->join('sys_user b','a.uid=b.uid')
            ->field(['a.listid','a.subject','a.private_mk','a.uid','a.mtime','date_format(`a`.`mtime`,\'%Y-%m-%d\')'=>'date','b.account as author'])
            ->where(['a.private_mk'=>'Y','a.uid'=>$uid])
            ->whereOr(['a.private_mk'=>'N'])
            ->page($page,$num)
            ->order('a.private_mk,a.mtime desc')
            ->select();
        $list = '';
        foreach ($data as $v){
            $icon = $v['private_mk'] == 'Y'? '<i class="fa fa-user-o text-info"></i>':'<i class="fa fa-globe text-success"></i>';
            $list .= '
            <a class="weui-cell'.($v['private_mk'] == 'Y'? ' weui-cell_access':'').'" href="javascript:;">
                <div class="weui-cell__bd">
                    <p>'.$icon.' '.$v['subject'].'</p>
                </div>
                <div class="weui-cell__ft" style="font-size: 0.78em;">'.($v['private_mk'] == 'Y'? '':$v['author'].'/').$v['date'].'</div>
            </a>
            ';
        }
        if($list) $page['dataList'] = $list;
        if($page) $this->assign('page',$page);
        return $this->fetch();
    }
}