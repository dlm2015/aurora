<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/22 0022 17:33
 * Email: brximl@163.com
 * Name: 财务计划
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc1000c;
use app\common\model\Fnc2000c;
use app\common\model\Fnc2001c;
use hyang\Bootstrap;

class Fplan extends Wap
{
    // 首页
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '财务计划'
        ]);
        $uid = $this->getUserInfo('uid');
        // 数据展示方式可选： 按条数/天数/月数/年等显示
        $fnc = new Fnc2000c();
        $subSql = (new Fnc1000c())->field('count(*)')->where('src_plan_no = a.plan_no')->buildSql();
        $data = $fnc
            ->alias('a')
            ->field(['a.*',$subSql=>'ctt'])
            ->where('a.uid',$uid)
            ->limit(20)
            ->select()
        ;
        $dataList = '';
        foreach ($data as $v){
            $endMk = $v['end_mk'];
            $dataList .= '
            <div class="weui-panel__bd">
                <div class="weui-media-box weui-media-box_text">
                    <h4 class="weui-media-box__title">
                        <i class="fa fa-clock-o'.($endMk == 'Y'? '':' text-success').'"></i>  '.$v['plan'].($v['ctt']? ' <span class="weui-badge">'.$v['ctt'].'</span>':'').'
                    </h4>
                    <p class="weui-media-box__desc">添加时间 '.$v['mtime'].'</p>
                    <ul class="weui-media-box__info">
                        <li class="weui-media-box__info__meta"><a href="'.url('fplan/edit','item='.$v['plan_no']).'">编辑</a></li>
                        '.($endMk == 'N' && !empty($v['cycle'])? '
                            <li class="weui-media-box__info__meta">
                                <a href="'.urlBuild('!.fplan/schedule','?src='.base64_encode($v['plan_no'])).'">财务排期</a>
                            </li>'
                        :'').'
                        <li class="weui-media-box__info__meta">
                            <a href="'.url('fplan/account_bind','item='.$v['plan_no']).'">账单绑定</a>
                        </li>
                    </ul>
                </div>
                <div class="weui-flex aurora-border"></div>
            </div>
            ';
        }
        if($dataList) $dataList = '<div class="weui-panel" style="min-height: 420px;">'.$dataList.'</div>';
        $dataList = $dataList? $dataList:'
        <div class="weui-cells" style="min-height: 420px;">
            <a class="weui-cell weui-cell_access" href="'.url('fplan/edit').'">
                <div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>
                <div class="weui-cell__ft">
                    您还没一条财务记录
                </div>
            </a>
        </div>
        ';
        $page = [];
        $page['list'] = $dataList;
        $this->assign('page',$page);
        return $this->fetch();
    }
    // 编辑
    public function edit(){
        $this->loadScript([
            'title' => '编辑 | 财务计划',
            'js'    => ['/lib/zepto/touch','fplan/edit']
        ]);
        $item = request()->param('item');
        $refPid = request()->param('ref_pid');
        $uid = $this->getUserInfo('uid');
        $data = [];
        $cycleUnit = 'M';
        if($item){
            $fnc = new Fnc2000c();
            //$data = $fnc->get($item)->toArray();
            $data = $fnc
                ->alias('a')
                ->field('a.*,b.title as pid_title')
                ->join(['fnc2001c'=>'b'],'a.pid = b.listid','left')
                ->where('a.plan_no',$item)
                ->find()
                ->toArray()
            ;
            if($uid != $data['uid']) $this->getErrorUrl();
            $cycleUnit = $data['cycle_unit'];
            $this->assign('f_pk_grid',Bootstrap::formPkGrid($data,'plan_no'));
        }elseif ($refPid){
            $fevMd = new Fnc2001c();
            $data = $fevMd->field('listid as pid,title as pid_title')->where('listid',$refPid)->find()->toArray();
        }
        $data['s_u_opts'] = Bootstrap::SelectGrid([
            'D' => '日例',
            'M' => '月份',
            'Y' => '年度'
        ],$cycleUnit);
        if($data) $this->assign('data',$data);
        return $this->fetch();
    }
    // 排期 - 根据周期数
    public function schedule(){
        $src = request()->param('src');
        $uid = $this->getUserInfo('uid');
        $content = '';
        if($src){
            $src = base64_decode($src);
            $fnc = new Fnc2000c();
            $data = $fnc->get($src)->toArray();
            if($uid != $data['uid']) $this->getErrorUrl();
            $content = '';
            $fnc->schedule($data,function($value,$type) use($data,&$content){
                static $isFirst = true;
                $content .= '
                    <div class="weui-cell">
                        <div class="weui-cell__bd">
                            <p>'.($isFirst? '<i class="fa fa-clock-o text-success"></i> ':'<i class="fa fa-clock-o"></i> ').$value.'</p>
                        </div>
                        <div class="weui-cell__ft">'.($data['standard_money']? $data['standard_money']:'无标准金').'</div>
                    </div>
                    ';
                $isFirst = false;
            });
        }
        if($content) $this->assign('content',$content);
        return $this->fetch();
    }
    // 账单绑定
    public function account_bind(){
        $this->checkAuth();
        $uid = $this->getUserInfo('uid');
        $this->loadScript([
            'title' => '编辑 | 账单绑定',
            'js'    => ['/lib/zepto/touch','fplan/account_bind']
        ]);
        $sname = request()->param('name');
        $planNo = request()->param('item');
        if(empty($planNo)) $this->getErrorUrl('请求参数无效！');
        $result = '';
        if($sname){
            $fnc = new Fnc1000c();
            $data = $fnc
                ->where('uid',$uid)
                ->where('name','like',"%$sname%")
                ->where('src_plan_no is null')
                ->limit(30)
                ->select()
                ;
            foreach ($data as $v){
                $text = $v['date'].' '.($v['name']? str_replace($sname,'<span style="color: red;">'.$sname.'</span>',$v['name']):'');
                $result .= '
                <a class="weui-cell weui-cell_access js__bind" href="javascript:;" data-no="'.base64_encode($v['no']).'">
                    <div class="weui-cell__bd weui-cell_primary">
                        <p>'.$text.'</p>
                    </div>
                </a>
                ';
            }
        }
        if(empty($result) && $sname) $result = '
            <div class="weui-cell weui-cell_access">
                <div class="weui-cell__bd weui-cell_primary">
                    <p class="text-danger">未搜索到相关的财务账单</p>
                </div>
            </div>
         ';
        $this->assign('result',$result);
        $this->_JsVar('plan_no',$planNo);
        $data = (new Fnc2000c())
            ->get($planNo)
            ->toArray();
        $tipXhtml = '
            <p>
            您正在进行【'.$data['plan'].'】账单绑定.
            </p>
           ';
        $this->assign('tipXhtml',$tipXhtml);
        return $this->fetch();
    }
}
