<?php
/**
 * Auther: Joshua Conero
 * Date: 2017/6/13 0013 22:02
 * Email: brximl@163.com
 * Name: 事务甲乙方
 */

namespace app\wap\controller;


use app\common\controller\Wap;
use app\common\model\Fnc0020c;
use app\common\model\Fnc1000c;
use hyang\Bootstrap;
use hyang\Util;

class Faffair extends Wap
{
    public function index(){
        $this->checkAuth();
        $this->loadScript([
            'title' => '甲乙事务 | 财务管理',
            'js'    => ['/lib/zepto/touch','faffair/index']
        ]);
        $uid = $this->getUserInfo('uid');
        $num = 12;
        $page = request()->param('page');
        $page = $page? intval($page):1;
        $fnc20 = new Fnc0020c();
        $count = $fnc20->where('uid',$uid)->count();
        $subSql = (new Fnc1000c())->field('count(*)')->where('master_id = a.listid or slave_id = a.listid')->buildSql();
        $data = $fnc20->where('uid',$uid)
            ->field(['a.*',$subSql=>'set_count'])
            ->alias('a')
            ->order('a.type,a.mtime')
            ->page($page,$num)
            ->select();
        $listCtt = '';
        foreach ($data as $v){
            if($v['type'] == 'M0') $icon = '<i class="fa fa-user text-success"></i> ';
            elseif ($v['type'] == 'S0') $icon = '<i class="fa fa-info-circle text-info"></i> ';
            else $icon = '<i class="fa fa-question text-primary"></i> ';
            $listCtt .= '
                <div class="weui-flex aurora-border"></div>
                <div class="weui-panel">
                    <div class="weui-panel__hd">
                    '.$icon.'<span style="font-size: 1.32em;color:#CC6600;margin-left:5px;">'.$v['name'].'</span> '.($v['set_count']? '('.$v['set_count'].')':'').'
                    <span style="float:right;font-style: italic;">'.$v['mtime'].'</span>
                    </div>
                    <div class="weui-panel__bd">
                        <div class="weui-media-box weui-media-box_small-appmsg" style="padding-left:50px;">
                            <div class="weui-cells">
                                <a class="weui-cell weui-cell_access" style="height:15px;font-size:0.78em;" href="'.url('faffair/edit','uid='.$v['listid']).'">
                                    <div class="weui-cell__hd"><i class="fa fa-pencil-square text-primary"></i></div>
                                    <div class="weui-cell__bd weui-cell_primary">
                                        <p>编辑</p>
                                    </div>
                                    <span class="weui-cell__ft"></span>
                                </a>       
                                 '.($v['set_count']? '':'
                                 <a class="weui-cell weui-cell_access row_del_lnk" style="height:15px;font-size:0.78em;" href="javascript:;" data-no="'.base64_encode($v['listid']).'">
                                    <div class="weui-cell__hd"><i class="fa fa-trash text-danger"></i></div>
                                    <div class="weui-cell__bd weui-cell_primary">
                                        <p>删除</p>
                                    </div>
                                    <span class="weui-cell__ft"></span>
                                </a>
                                ').'
                            </div>
                        </div>
                    </div>
                </div>
            ';
        }
        if($listCtt) $this->assign('listCtt',$listCtt);
        $pageData = [];
        $pageData['count'] = $count;
        $pageData['num'] = $num;
        $pageData['page'] = $page;
        $pageData['all_page'] = Util::getAllPage($count,$num);
        $this->assign('data',$pageData);
        // 前端变量
        $this->_JsVar('page',[
            'page' => $page,
            'all_page' => $pageData['all_page']
        ]);
        return $this->fetch();
    }
    // 编辑页面
    public function edit(){
        $this->checkAuth();
        $this->loadScript([
            'js' => ['/lib/zepto/touch','faffair/edit']
        ]);
        $listid = request()->param('uid');
        $uid = $this->getUserInfo('uid');
        $fnc20x = new Fnc0020c();
        $select = null;$checkd=null;
        if($listid){
            $data = $fnc20x->get($listid)->toArray();
            if($data['uid'] != $uid){
                $this->getErrorUrl('请求参数有误!');
            }
            $this->assign('data',$data);
            $select = $data['type'];
            if($data['use_mk'] == 'N') $checkd = true;
            $this->assign('pk_form',Bootstrap::formPkGrid($data));
        }
        // 选取最近设置的十个
        $qdata = $fnc20x->where('uid',$uid)
            ->where('group_mk','not null')
            ->field('group_mk')
            ->order('mtime desc')
            ->group('group_mk')
            ->limit(10)
            ->select();
        $groups = [];
        foreach ($qdata as $v){
            $groups[] = $v['group_mk'];
        }
        $page = [];
        if($groups){
            $this->_JsVar('groups',$groups);
            $page['group_sel_able'] = 'Y';
        }
        $typeSel = Bootstrap::SelectGrid([
            'M0' => '事务甲方',
            'S0' => '事务乙方',
            '00' => '未区分',
        ],$select);
        // <input id="usemk_ipter" class="weui-switch-cp__input" name="use_mk" value="Y" type="checkbox" checked="checked">
        $page['use_mk'] = Bootstrap::RadioGrid('id="usemk_ipter" class="weui-switch-cp__input" name="use_mk" value="N"',$checkd);
        $page['typeSel'] = $typeSel;
        // 辅助操作
        $this->assign('page',$page);
        return $this->fetch();
    }
}