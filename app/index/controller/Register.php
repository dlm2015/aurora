<?php
/**
 * Auther: Joshua Conero
 * Email: brximl@163.com
 * Name: web 登陆页面
 * date: 2017年7月18日 星期二
 */
namespace app\index\controller;
use app\common\controller\Web;

class Register extends Web
{
    public function index(){
        $this->loadScript([
            'js' => ['register/index'],
            'title' => '用户注册'
        ]);
        return $this->fetch();
    }

}