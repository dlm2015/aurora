<?php
/**
 * Auther: Joshua Conero
 * Email: brximl@163.com
 * Name: web 登陆页面
 * date: 2017年7月18日 星期二
 */
namespace app\index\controller;
use app\common\controller\Web;

class Login extends Web
{
    public function index(){
        // 登录状态监测
        $user = $this->getUserInfo('user');
        if($user){
            $this->getErrorUrl("您已经登系统了. Y'Know you don't never to do it again, right?");
        }
        $this->loadScript([
            'js' => ['login/index'],
            'title' => '登陆系统'
        ]);
        return $this->fetch();
    }
}