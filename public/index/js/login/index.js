/**
 * 2017年7月18日 星期二
 * 用户登录
 */
$(function(){
    $('[data-toggle="tooltip"]').tooltip();
    // 验证码刷新
    function refreshCode(dom) {
        /*
        // google 浏览器有效，ie，Firefox 无效
        var img = dom? dom.find('img'):$('#recode_lnk').find('img');
        var src = img.attr("src");
        img.attr('src',src);
        */
        dom = dom? dom:$('#recode_lnk');
        var xhtml = '<img class="weui-vcode-img" src="/aurora/captcha?image='+(Math.random()*100)+'">';
        dom.html(xhtml);
        //console.log(src,img);
    }
    refreshCode();
    // 验证码更换
    $('#recode_lnk').click(function () {
        refreshCode($(this));
    });
    // 数据提交
    $('button[type="submit"]').click(function () {
        var data = Web.formJson('form');
        /*
        if('' == data.account){
            Web.modal_alert('请输入用户名！');
        }else if('' == data.pswd){
            Web.modal_alert('请输入密码！');
        }else if('' == data.code){
            Web.modal_alert('请输入验证码！');
        }else{}
        */
        if('' != data.account && '' != data.pswd && '' != data.code){
            var loading = Web.loading();
            Web.ApiRequest('login/auth',data,function (rdata) {
                loading.hide();
                if(rdata.code == 1){
                    refreshCode();
                    Web.modal_alert(rdata.msg);
                    setTimeout(function () {
                        location.href = '/center/bin';
                    },3000);
                }
                else{
                    Web.modal_alert(rdata.msg);
                }
            });
        }
    });
});