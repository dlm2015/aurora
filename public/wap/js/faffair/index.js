/**
 * Created by Administrator on 2017/6/20 0020.
 */
$(function () {
    var argv = Wap.getJsVar('page');
    // 右滑动时返回或上一页
    $(document).swipeRight(function () {
        // 上一页
        if(argv.page != argv.all_page && argv.page > 1){
            location.href = Wap._wapurl + 'faffair.html?page='+(argv.page - 1);
            return;
        }
        if(typeof callback == 'function'){
            if(!callback()) return;
        }
        location.href = Wap._wapurl + 'finance.html';
    });
    // 下一页
    $(document).swipeRight(function () {
        if(argv.page < argv.all_page){
            location.href = Wap._wapurl + 'faffair.html?page='+(argv.page + 1);
        }else{
            weui.topTips('没有更多数据了，朋友！');
        }
    });
    // 删除数据
    $('.row_del_lnk').click(function () {
        var dom = $(this);
        var listid = dom.data('no');
        weui.confirm('您确定要删除数据吗？',function () {
            var loadding = weui.loading('数据提交中……');
            Wap.ApiRequest('faffair/save',{mode:'D',listid:listid},function (rdata) {
               loadding.hide();
               if(data.code == 1) dom.remove();
               weui.topTips(data.msg);
           });
       });
    });
});