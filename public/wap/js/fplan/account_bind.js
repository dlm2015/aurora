/**
 * Created by Administrator on 2017/7/2 0002.
 */
$(function () {
    var planNo = Wap.getJsVar('plan_no');
    // 右滑动时返回
    Wap.SwipeRightBack('fplan.html');
    // 搜索框
    weui.searchBar('#searchBar');
    // 绑定操作
    $('.js__bind').click(function () {
        var dom = $(this);
        var no = dom.data('no');
        if(no){
            Wap.ApiRequest('faccount/save',{mode:'M',pk:no,src_plan_no:planNo},function (rdata) {
                if(rdata.code == 1){
                    weui.toast('账单绑定成功');
                    dom.remove();
                }
                else{
                    weui.alert('账单绑定失败!');
                }
            });
        }
    });
});