/**
 * Created by Administrator on 2017/7/2 0002.
 */
$(function () {
    var detailNo = null; // 明细账单号， Y(禁止保存),no 可修改，
    // 右滑动时返回
    Wap.SwipeRightBack('faccount.html');
    function exchangeBar(target) {
        var dom = $('div.aurora-hidden');
        var id = dom.attr('id');
        //var queue = ['list','edit'];
        //dom.removeClass('aurora-hidden');
        $('div.weui-tab__panel').addClass('aurora-hidden');
        target = target? target:'#'+id;
        $(target).removeClass('aurora-hidden');
        location.href = target;
    }
    // 新增明细链接
    $('#add_detail_lnk').click(function () {
        detailNo = null;
        // 移除删除栏
        var lnk = $('#row_del_lnk');
        if(lnk.length > 0) lnk.remove();
        Wap.ResetForm('#edit',['amount']);
        exchangeBar();
    });
    function delBtn() {
        var form = $('#edit').find('.weui-cells_form');
        var lnk = form.find('#row_del_lnk');
        if(lnk.length == 0 && (detailNo && 'Y' != detailNo)){
            var xhtml = '<a class="weui-cell weui-cell_access" href="javascript:;" id="row_del_lnk">'
                + '<div class="weui-cell__bd text-warning">'
                + '<p>删除该明细</p>'
                + '</div>'
                + '<div class="weui-cell__ft"></div>'
                + '</a>';
            form.append(xhtml);
            $('#row_del_lnk').off('click').on('click',function () {
               weui.confirm('您确定要删除该账单明细吗？',function () {
                    Wap.ApiRequest('faccount/detail',{mode:'D','no':Base64.encode(detailNo)},function (rdata) {
                        if(rdata.code == '1') $('a[data-no="'+detailNo+'"]').remove();
                        weui.topTips(rdata.msg);
                    });
               });
            });
        }
    }
    // 账单修改调整
    $('#list').find('a[data-no]').click(function () {
       var id = $(this).data('no');
        Wap.ApiRequest('faccount/get_detail',{no:id},function (rdata) {
           if(rdata.code == 1){
               exchangeBar('#edit');
               var data = rdata.data;
               $('#name_ipter').val(data.name);
               $('#money_ipter').val(data.money);
               $('#amount_ipter').val(data.amount);
               $('#remark_ipter').val(data.remark);
               detailNo = id;
               delBtn();
           }
            else{
               weui.alert(rdata.msg);
           }
        });
    });
    // 表单提交
    var srcNo = Wap.getUrlBind('item');
    var ListSum = parseFloat($('#sum_col').text());
    weui.form.checkIfBlur('#edit');
    $('#edit_submit_lnk').click(function () {
        if(Wap.empty(srcNo)){
            weui.topTips('请求参数获取失败，无法保存数据！');
            return;
        }
        if('Y' === detailNo){
            weui.topTips('该数据禁止保存！');
            return;
        }
        var loading = weui.loading('数据提交中……');
        weui.form.validate('#edit', function (error) {
            if(!error){
                var savedata = Wap.formJson('#edit');
                if(detailNo){savedata.no = Base64.encode(detailNo);}
                else savedata.src_no = srcNo;
                Wap.ApiRequest('faccount/detail',savedata,function (data) {
                    loading.hide();
                    if(data.code == -1){
                        weui.alert(data.msg);
                        return null;
                    }
                    if(detailNo){
                        var aEl = $('a[data-no="'+detailNo+'"]');
                        if(aEl.length > 0){
                            aEl.find('div.weui-cell__bd > p').text(savedata.name);
                            aEl.find('weui-cell__ft').html('<span style="color:#FF6633;">' + (parseFloat(savedata.money) * parseInt(savedata.amount)) +
                                '</span> (' + savedata.money + ' * ' + savedata.amount + ')');
                        }
                    }
                    else {
                        var listXhtml = '<a class="weui-cell weui-cell_access" href="javascript:;">' +
                                '<div class="weui-cell__bd">' +
                                '<p>' + savedata.name + '</p>' +
                                '</div>' +
                                '<div class="weui-cell__ft"><span style="color:#FF6633;">' +
                                (parseFloat(savedata.money) * parseInt(savedata.amount)) +
                                '</span> (' + savedata.money + ' * ' + savedata.amount + ')' +
                                '</div>' +
                                '</a>'
                            ;
                        $('#list').find('.weui-cells').append(listXhtml);
                    }
                    ListSum = ListSum + (parseFloat(savedata.money) * parseFloat(savedata.amount));
                    $('#sum_col').text(ListSum);
                    exchangeBar('#list');
                    Wap.ResetForm('#edit',['amount']);
                    weui.toast(data.msg);

                });
            }else loading.hide();
        });
    });

    // 自动适应
    exchangeBar(location.hash);
});