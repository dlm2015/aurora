/**
 * Created by Administrator on 2017/6/20 0020.
 */
$(function () {
    var curPage = 1,     // 当前列
        allPage,             // 总列数
        searchValue = ''
        ;
    // 删除财务账单确定绑定事件
    function delCkeckEvt() {
        var dom = $(this);
        weui.confirm('您确定要删除此财务账单吗?', function(){
            var no = dom.parents('div.weui-media-box').data('no');
            Wap.ApiRequest('faccount/save',{mode:'D','no':no},function (rdata) {
                if(rdata.code == 1){
                    dom.parents('div.weui-panel__bd').remove();
                    weui.toast(rdata.msg);
                }
                else{
                    weui.alert(rdata.msg);
                }
            });
        });
    }
    /**
     * 数据渲染
     * @param result
     * @returns {string}
     */
    function dataToList(result) {
        var xhtml = '';
        for(var i=0; i<result.length; i++){
            var v = result[i];
            xhtml += '<div class="weui-panel__bd">' +
                '<div class="weui-media-box weui-media-box_text" data-no="'+ Base64.encode(v.no)+'">' +
                '<h4 class="weui-media-box__title">' +
                '<i class="fa ' + (v.type == 'IN'? 'fa-plus-circle text-success':'fa-minus-circle text-info') + '"></i> '+
                v.date+ '<span style="font-size: 0.78em;font-style: italic;">('+Wap.numberToZh(v.week,'dxq')+')</span> <span style="color:#CC6600;">' +
                v.money+ '</span>' + (v.name? ' <span style="color: #880000;font-size: 0.87em;font-style: italic;">' + v.name + '</span>':'') +
                '</h4>' +
                '<p class="weui-media-box__desc">'+ v.descrip + '</p>' +
                '<ul class="weui-media-box__info">' +
                '<li class="weui-media-box__info__meta"> '+ v.mtime + '</li>' +
                '<li class="weui-media-box__info__meta">' +
                '<a href="'+ Wap._wapurl + 'faccount/edit/item/' + v.no + '" style="color:#669966;"><i class="fa fa-pencil-square"></i> 编辑</a>' +
                '</li>' +
                '<li class="weui-media-box__info__meta">' +
                '<a href="'+ Wap._wapurl + 'faccount/edit?tpl=' + v.no + '" style="color:#666666;"><i class="fa fa-plus-circle"></i> 模板新增</a>' +
                '</li>' +
                '<li class="weui-media-box__info__meta">' +
                (v.detail_ctt ? '<a href="'+ Wap._wapurl + 'faccount/detail/item/' + v.no + '.html" style="color:#CCCC00;"><span class="weui-badge">' + v.detail_ctt + '</span>':'<a href="'+ Wap._wapurl + 'faccount/detail/item/' + v.no + '" style="color:#CCCCCC;"><i class="fa fa-plus-circle"></i>') +
                '财务明细</a>' +
                '</li>' +
                '<li class="weui-media-box__info__meta">' +
                '<a href="javascript:void(0);" class="js__item_del text-danger"><i class="fa fa-trash-o"></i> 删除</a>' +
                '</li>' +
                '' +
                '</ul>' +
                '</div>' +
                '<div class="weui-flex aurora-border"></div>' +
                '</div>'
            ;
        }
        return xhtml;
    }
    // 获取财务账单
    function getSets(page) {
        console.log(allPage,curPage);
        if(allPage && allPage < curPage){
            return weui.topTips('没有更多数据了！');
        }
        page = page? parseInt(page):1;
        var loading = weui.loading('数据加载中……');
        Wap.ApiRequest('faccount/sets',{page:page},function (rdata) {
            loading.hide();
            if(rdata.code == 1){
                var data = rdata.data;
                var result = data.result;
                curPage = data.page;
                $('#sets_count').text(data.count);
                // 总列数计算
                if(!allPage) allPage = Wap.getAllPage(data.count,data.num);
                var xhtml = dataToList(result);
                if('' != xhtml){
                    var xhtmlObj = $(xhtml);
                    xhtmlObj.find('.js__item_del').click(delCkeckEvt);
                    $('#finance_set').append(xhtmlObj);
                }else{
                    xhtml =
                        '<div class="weui-cells" style="min-height: 420px;">'
                        + '<a class="weui-cell weui-cell_access" href="'+ Wap._wapurl + 'faccount/edit'+ '">'
                        + '<div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>'
                        + '<div class="weui-cell__ft">'
                        + '您还没一条财务记录'
                        + '</div>'
                        + '</a>'
                        + '</div>';
                    $('#finance_set').append(xhtml);
                }
            }else{
                weui.topTips(rdata.msg);
            }
        });
    }
    // 右滑动时返回
    Wap.SwipeRightBack();
    getSets();
    // (上拖)上滑动 - 数据翻页
    $('.weui-footer').swipeUp(function () {
       if(curPage < allPage){
           curPage = curPage + 1;
           getSets(curPage);
       }else{
           weui.topTips('没有更多数据！');
       }
    });
    // 更多数据加载
    $('#load_more_lnk').click(function () {
        if(curPage < allPage){
            curPage = curPage + 1;
            getSets(curPage);
        }else{
            weui.topTips('没有更多数据！');
            $(this).parents('div.weui-flex').hide();
        }
    });
    // 搜索框
    $('#search_lnk').click(function () {
        var dialog;
        var content = '<div class="weui-cells__title">请输入搜索值</div>'
            + '<div class="weui-cells">'
            + '<div class="weui-cell">'
                + '<input type="text" class="weui-input" value="'+searchValue+'">'
            + '</div>'
            + '</div>'
            ;
        dialog = weui.dialog({
            //title: '搜索框',
            content: content,
            className: 'search_dialog',
            buttons: [{
                label: '关闭',
                type: 'default',
                onClick: function () {dialog.hide();}
            }, {
                label: '搜索',
                type: 'primary',
                onClick: function () {
                    var value = $('.search_dialog input.weui-input').val();
                    if(value){
                        searchValue = value;
                        var loading = weui.loading('正在搜索中……');
                        Wap.ApiRequest('faccount/search',{search:value},function (rdata) {
                            loading.hide();
                            var xhtml = '';
                            if(1 == rdata.code) {
                                var data = rdata.data;
                                xhtml = dataToList(data.result);
                                $('#sets_count').text(data.count);
                            }
                            if('' != xhtml){
                                var xhtmlObj = $(xhtml);
                                xhtmlObj.find('.js__item_del').click(delCkeckEvt);
                                $('#finance_set').html(xhtmlObj);
                            }else{
                                xhtml =
                                    '<div class="weui-cells" style="min-height: 420px;">'
                                    + '<a class="weui-cell" href="javascript:;">'
                                    + '<div class="weui-cell__bd"><i class="fa fa-warning text-danger"></i></div>'
                                    + '<div class="weui-cell__ft">'
                                    + '没有找到有关“'+ searchValue +'”的账单！！'
                                    + '</div>'
                                    + '</a>'
                                    + '</div>';
                                $('#finance_set').html(xhtml);
                                $('#sets_count').text('0');
                            }
                        });
                    }
                    else{
                        setTimeout(function () {
                            weui.topTips('搜索内容空！');
                        },1000);
                        dialog.hide();
                    }
                }
            }]
        });
    });
});